﻿namespace Bibliogram
{
    partial class FrmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CmbWybór = new System.Windows.Forms.ComboBox();
            this.TxtImie = new System.Windows.Forms.TextBox();
            this.TxtNazwisko = new System.Windows.Forms.TextBox();
            this.TxtTytul = new System.Windows.Forms.TextBox();
            this.TxtMiejsce = new System.Windows.Forms.TextBox();
            this.TxtRok = new System.Windows.Forms.TextBox();
            this.TxtWydawnictwo = new System.Windows.Forms.TextBox();
            this.TxtStrona = new System.Windows.Forms.TextBox();
            this.TxtTytulCzasopisma = new System.Windows.Forms.TextBox();
            this.TxtNumerCzasopisma = new System.Windows.Forms.TextBox();
            this.TxtProd = new System.Windows.Forms.TextBox();
            this.TxtNosnik = new System.Windows.Forms.TextBox();
            this.LblInicjal = new System.Windows.Forms.Label();
            this.LblNazwisko = new System.Windows.Forms.Label();
            this.LblTytul = new System.Windows.Forms.Label();
            this.LblMiejsceWydania = new System.Windows.Forms.Label();
            this.LblRokWydania = new System.Windows.Forms.Label();
            this.LblWydawnictwo = new System.Windows.Forms.Label();
            this.LblNumerStronyKsiazki = new System.Windows.Forms.Label();
            this.LblTytulCzasopisma = new System.Windows.Forms.Label();
            this.LblNumerCzasopisma = new System.Windows.Forms.Label();
            this.LblProducent = new System.Windows.Forms.Label();
            this.BtnTworzenie = new System.Windows.Forms.Button();
            this.RichPrzypis = new System.Windows.Forms.RichTextBox();
            this.LblTypNosnika = new System.Windows.Forms.Label();
            this.TxtStronaCzasopisma = new System.Windows.Forms.TextBox();
            this.LblNumerStronyCzasopisma = new System.Windows.Forms.Label();
            this.LblRokProdukcji = new System.Windows.Forms.Label();
            this.LblMiejsceProdukcji = new System.Windows.Forms.Label();
            this.BtnClean = new System.Windows.Forms.Button();
            this.TxtRokProdukcji = new System.Windows.Forms.TextBox();
            this.TxtImieCz = new System.Windows.Forms.TextBox();
            this.TxtNazwiskoCz = new System.Windows.Forms.TextBox();
            this.TxtTytulArtykulu = new System.Windows.Forms.TextBox();
            this.TxtRokWydaniaCzasopisma = new System.Windows.Forms.TextBox();
            this.LblImieCzasopismo = new System.Windows.Forms.Label();
            this.LblNazwiskoCzasopismo = new System.Windows.Forms.Label();
            this.LblTytulArtykulu = new System.Windows.Forms.Label();
            this.LblRokWydaniaCzasopisma = new System.Windows.Forms.Label();
            this.TxtImieFilm = new System.Windows.Forms.TextBox();
            this.TxtNazwiskoFilm = new System.Windows.Forms.TextBox();
            this.TxtTytulFilm = new System.Windows.Forms.TextBox();
            this.TxtMiejsceProdukcji = new System.Windows.Forms.TextBox();
            this.LblInicjalFilm = new System.Windows.Forms.Label();
            this.LblNazwiskoFilm = new System.Windows.Forms.Label();
            this.LblTytulFilmu = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // CmbWybór
            // 
            this.CmbWybór.FormattingEnabled = true;
            this.CmbWybór.Items.AddRange(new object[] {
            "Książka",
            "Czasopismo",
            "Film"});
            this.CmbWybór.Location = new System.Drawing.Point(234, 12);
            this.CmbWybór.Name = "CmbWybór";
            this.CmbWybór.Size = new System.Drawing.Size(254, 21);
            this.CmbWybór.TabIndex = 0;
            this.CmbWybór.SelectedIndexChanged += new System.EventHandler(this.CmbWybór_SelectedIndexChanged);
            // 
            // TxtImie
            // 
            this.TxtImie.Location = new System.Drawing.Point(12, 81);
            this.TxtImie.Name = "TxtImie";
            this.TxtImie.Size = new System.Drawing.Size(189, 20);
            this.TxtImie.TabIndex = 1;
            // 
            // TxtNazwisko
            // 
            this.TxtNazwisko.Location = new System.Drawing.Point(12, 123);
            this.TxtNazwisko.Name = "TxtNazwisko";
            this.TxtNazwisko.Size = new System.Drawing.Size(189, 20);
            this.TxtNazwisko.TabIndex = 2;
            // 
            // TxtTytul
            // 
            this.TxtTytul.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.TxtTytul.Location = new System.Drawing.Point(12, 160);
            this.TxtTytul.Name = "TxtTytul";
            this.TxtTytul.Size = new System.Drawing.Size(189, 20);
            this.TxtTytul.TabIndex = 3;
            // 
            // TxtMiejsce
            // 
            this.TxtMiejsce.Location = new System.Drawing.Point(12, 205);
            this.TxtMiejsce.Name = "TxtMiejsce";
            this.TxtMiejsce.Size = new System.Drawing.Size(189, 20);
            this.TxtMiejsce.TabIndex = 4;
            // 
            // TxtRok
            // 
            this.TxtRok.Location = new System.Drawing.Point(12, 245);
            this.TxtRok.Name = "TxtRok";
            this.TxtRok.Size = new System.Drawing.Size(189, 20);
            this.TxtRok.TabIndex = 5;
            // 
            // TxtWydawnictwo
            // 
            this.TxtWydawnictwo.Location = new System.Drawing.Point(12, 281);
            this.TxtWydawnictwo.Name = "TxtWydawnictwo";
            this.TxtWydawnictwo.Size = new System.Drawing.Size(189, 20);
            this.TxtWydawnictwo.TabIndex = 6;
            // 
            // TxtStrona
            // 
            this.TxtStrona.Location = new System.Drawing.Point(12, 333);
            this.TxtStrona.Name = "TxtStrona";
            this.TxtStrona.Size = new System.Drawing.Size(189, 20);
            this.TxtStrona.TabIndex = 7;
            // 
            // TxtTytulCzasopisma
            // 
            this.TxtTytulCzasopisma.Location = new System.Drawing.Point(234, 205);
            this.TxtTytulCzasopisma.Name = "TxtTytulCzasopisma";
            this.TxtTytulCzasopisma.Size = new System.Drawing.Size(189, 20);
            this.TxtTytulCzasopisma.TabIndex = 8;
            // 
            // TxtNumerCzasopisma
            // 
            this.TxtNumerCzasopisma.Location = new System.Drawing.Point(234, 294);
            this.TxtNumerCzasopisma.Name = "TxtNumerCzasopisma";
            this.TxtNumerCzasopisma.Size = new System.Drawing.Size(189, 20);
            this.TxtNumerCzasopisma.TabIndex = 9;
            // 
            // TxtProd
            // 
            this.TxtProd.Location = new System.Drawing.Point(492, 245);
            this.TxtProd.Name = "TxtProd";
            this.TxtProd.Size = new System.Drawing.Size(189, 20);
            this.TxtProd.TabIndex = 10;
            // 
            // TxtNosnik
            // 
            this.TxtNosnik.Location = new System.Drawing.Point(492, 333);
            this.TxtNosnik.Name = "TxtNosnik";
            this.TxtNosnik.Size = new System.Drawing.Size(189, 20);
            this.TxtNosnik.TabIndex = 11;
            // 
            // LblInicjal
            // 
            this.LblInicjal.AutoSize = true;
            this.LblInicjal.Location = new System.Drawing.Point(12, 55);
            this.LblInicjal.Name = "LblInicjal";
            this.LblInicjal.Size = new System.Drawing.Size(71, 13);
            this.LblInicjal.TabIndex = 14;
            this.LblInicjal.Text = "Inicjał imienia";
            // 
            // LblNazwisko
            // 
            this.LblNazwisko.AutoSize = true;
            this.LblNazwisko.Location = new System.Drawing.Point(9, 107);
            this.LblNazwisko.Name = "LblNazwisko";
            this.LblNazwisko.Size = new System.Drawing.Size(53, 13);
            this.LblNazwisko.TabIndex = 15;
            this.LblNazwisko.Text = "Nazwisko";
            // 
            // LblTytul
            // 
            this.LblTytul.AutoSize = true;
            this.LblTytul.Location = new System.Drawing.Point(9, 144);
            this.LblTytul.Name = "LblTytul";
            this.LblTytul.Size = new System.Drawing.Size(32, 13);
            this.LblTytul.TabIndex = 16;
            this.LblTytul.Text = "Tytuł";
            // 
            // LblMiejsceWydania
            // 
            this.LblMiejsceWydania.AutoSize = true;
            this.LblMiejsceWydania.Location = new System.Drawing.Point(9, 189);
            this.LblMiejsceWydania.Name = "LblMiejsceWydania";
            this.LblMiejsceWydania.Size = new System.Drawing.Size(85, 13);
            this.LblMiejsceWydania.TabIndex = 17;
            this.LblMiejsceWydania.Text = "Miejsce wydania";
            // 
            // LblRokWydania
            // 
            this.LblRokWydania.AutoSize = true;
            this.LblRokWydania.Location = new System.Drawing.Point(9, 229);
            this.LblRokWydania.Name = "LblRokWydania";
            this.LblRokWydania.Size = new System.Drawing.Size(69, 13);
            this.LblRokWydania.TabIndex = 18;
            this.LblRokWydania.Text = "Rok wydania";
            // 
            // LblWydawnictwo
            // 
            this.LblWydawnictwo.AutoSize = true;
            this.LblWydawnictwo.Location = new System.Drawing.Point(12, 268);
            this.LblWydawnictwo.Name = "LblWydawnictwo";
            this.LblWydawnictwo.Size = new System.Drawing.Size(74, 13);
            this.LblWydawnictwo.TabIndex = 19;
            this.LblWydawnictwo.Text = "Wydawnictwo";
            // 
            // LblNumerStronyKsiazki
            // 
            this.LblNumerStronyKsiazki.AutoSize = true;
            this.LblNumerStronyKsiazki.Location = new System.Drawing.Point(9, 317);
            this.LblNumerStronyKsiazki.Name = "LblNumerStronyKsiazki";
            this.LblNumerStronyKsiazki.Size = new System.Drawing.Size(104, 13);
            this.LblNumerStronyKsiazki.TabIndex = 20;
            this.LblNumerStronyKsiazki.Text = "Numer strony książki";
            // 
            // LblTytulCzasopisma
            // 
            this.LblTytulCzasopisma.AutoSize = true;
            this.LblTytulCzasopisma.Location = new System.Drawing.Point(231, 189);
            this.LblTytulCzasopisma.Name = "LblTytulCzasopisma";
            this.LblTytulCzasopisma.Size = new System.Drawing.Size(90, 13);
            this.LblTytulCzasopisma.TabIndex = 21;
            this.LblTytulCzasopisma.Text = "Tytuł czasopisma";
            // 
            // LblNumerCzasopisma
            // 
            this.LblNumerCzasopisma.AutoSize = true;
            this.LblNumerCzasopisma.Location = new System.Drawing.Point(231, 278);
            this.LblNumerCzasopisma.Name = "LblNumerCzasopisma";
            this.LblNumerCzasopisma.Size = new System.Drawing.Size(96, 13);
            this.LblNumerCzasopisma.TabIndex = 22;
            this.LblNumerCzasopisma.Text = "Numer czasopisma";
            // 
            // LblProducent
            // 
            this.LblProducent.AutoSize = true;
            this.LblProducent.Location = new System.Drawing.Point(489, 229);
            this.LblProducent.Name = "LblProducent";
            this.LblProducent.Size = new System.Drawing.Size(56, 13);
            this.LblProducent.TabIndex = 25;
            this.LblProducent.Text = "Producent";
            // 
            // BtnTworzenie
            // 
            this.BtnTworzenie.Location = new System.Drawing.Point(165, 373);
            this.BtnTworzenie.Name = "BtnTworzenie";
            this.BtnTworzenie.Size = new System.Drawing.Size(152, 34);
            this.BtnTworzenie.TabIndex = 26;
            this.BtnTworzenie.Text = "Twórz przypis";
            this.BtnTworzenie.UseVisualStyleBackColor = true;
            this.BtnTworzenie.Click += new System.EventHandler(this.BtnTworzenie_Click);
            // 
            // RichPrzypis
            // 
            this.RichPrzypis.Location = new System.Drawing.Point(32, 414);
            this.RichPrzypis.Name = "RichPrzypis";
            this.RichPrzypis.Size = new System.Drawing.Size(649, 23);
            this.RichPrzypis.TabIndex = 27;
            this.RichPrzypis.Text = "";
            this.RichPrzypis.TextChanged += new System.EventHandler(this.RichPrzypis_TextChanged);
            // 
            // LblTypNosnika
            // 
            this.LblTypNosnika.AutoSize = true;
            this.LblTypNosnika.Location = new System.Drawing.Point(489, 317);
            this.LblTypNosnika.Name = "LblTypNosnika";
            this.LblTypNosnika.Size = new System.Drawing.Size(65, 13);
            this.LblTypNosnika.TabIndex = 31;
            this.LblTypNosnika.Text = "Typ nośnika";
            // 
            // TxtStronaCzasopisma
            // 
            this.TxtStronaCzasopisma.Location = new System.Drawing.Point(234, 333);
            this.TxtStronaCzasopisma.Name = "TxtStronaCzasopisma";
            this.TxtStronaCzasopisma.Size = new System.Drawing.Size(189, 20);
            this.TxtStronaCzasopisma.TabIndex = 32;
            // 
            // LblNumerStronyCzasopisma
            // 
            this.LblNumerStronyCzasopisma.AutoSize = true;
            this.LblNumerStronyCzasopisma.Location = new System.Drawing.Point(231, 317);
            this.LblNumerStronyCzasopisma.Name = "LblNumerStronyCzasopisma";
            this.LblNumerStronyCzasopisma.Size = new System.Drawing.Size(127, 13);
            this.LblNumerStronyCzasopisma.TabIndex = 33;
            this.LblNumerStronyCzasopisma.Text = "Numer strony czasopisma";
            // 
            // LblRokProdukcji
            // 
            this.LblRokProdukcji.AutoSize = true;
            this.LblRokProdukcji.Location = new System.Drawing.Point(489, 281);
            this.LblRokProdukcji.Name = "LblRokProdukcji";
            this.LblRokProdukcji.Size = new System.Drawing.Size(73, 13);
            this.LblRokProdukcji.TabIndex = 34;
            this.LblRokProdukcji.Text = "Rok produkcji";
            // 
            // LblMiejsceProdukcji
            // 
            this.LblMiejsceProdukcji.AutoSize = true;
            this.LblMiejsceProdukcji.Location = new System.Drawing.Point(489, 189);
            this.LblMiejsceProdukcji.Name = "LblMiejsceProdukcji";
            this.LblMiejsceProdukcji.Size = new System.Drawing.Size(89, 13);
            this.LblMiejsceProdukcji.TabIndex = 35;
            this.LblMiejsceProdukcji.Text = "Miejsce produkcji";
            // 
            // BtnClean
            // 
            this.BtnClean.Location = new System.Drawing.Point(333, 373);
            this.BtnClean.Name = "BtnClean";
            this.BtnClean.Size = new System.Drawing.Size(155, 33);
            this.BtnClean.TabIndex = 36;
            this.BtnClean.Text = "Wyczyść";
            this.BtnClean.UseVisualStyleBackColor = true;
            this.BtnClean.Click += new System.EventHandler(this.BtnClean_Click);
            // 
            // TxtRokProdukcji
            // 
            this.TxtRokProdukcji.Location = new System.Drawing.Point(492, 294);
            this.TxtRokProdukcji.Name = "TxtRokProdukcji";
            this.TxtRokProdukcji.Size = new System.Drawing.Size(189, 20);
            this.TxtRokProdukcji.TabIndex = 37;
            // 
            // TxtImieCz
            // 
            this.TxtImieCz.Location = new System.Drawing.Point(234, 81);
            this.TxtImieCz.Name = "TxtImieCz";
            this.TxtImieCz.Size = new System.Drawing.Size(189, 20);
            this.TxtImieCz.TabIndex = 38;
            // 
            // TxtNazwiskoCz
            // 
            this.TxtNazwiskoCz.Location = new System.Drawing.Point(234, 123);
            this.TxtNazwiskoCz.Name = "TxtNazwiskoCz";
            this.TxtNazwiskoCz.Size = new System.Drawing.Size(189, 20);
            this.TxtNazwiskoCz.TabIndex = 39;
            // 
            // TxtTytulArtykulu
            // 
            this.TxtTytulArtykulu.Location = new System.Drawing.Point(234, 160);
            this.TxtTytulArtykulu.Name = "TxtTytulArtykulu";
            this.TxtTytulArtykulu.Size = new System.Drawing.Size(189, 20);
            this.TxtTytulArtykulu.TabIndex = 40;
            // 
            // TxtRokWydaniaCzasopisma
            // 
            this.TxtRokWydaniaCzasopisma.Location = new System.Drawing.Point(234, 245);
            this.TxtRokWydaniaCzasopisma.Name = "TxtRokWydaniaCzasopisma";
            this.TxtRokWydaniaCzasopisma.Size = new System.Drawing.Size(189, 20);
            this.TxtRokWydaniaCzasopisma.TabIndex = 41;
            // 
            // LblImieCzasopismo
            // 
            this.LblImieCzasopismo.AutoSize = true;
            this.LblImieCzasopismo.Location = new System.Drawing.Point(231, 65);
            this.LblImieCzasopismo.Name = "LblImieCzasopismo";
            this.LblImieCzasopismo.Size = new System.Drawing.Size(71, 13);
            this.LblImieCzasopismo.TabIndex = 42;
            this.LblImieCzasopismo.Text = "Inicjał imienia";
            // 
            // LblNazwiskoCzasopismo
            // 
            this.LblNazwiskoCzasopismo.AutoSize = true;
            this.LblNazwiskoCzasopismo.Location = new System.Drawing.Point(231, 107);
            this.LblNazwiskoCzasopismo.Name = "LblNazwiskoCzasopismo";
            this.LblNazwiskoCzasopismo.Size = new System.Drawing.Size(53, 13);
            this.LblNazwiskoCzasopismo.TabIndex = 43;
            this.LblNazwiskoCzasopismo.Text = "Nazwisko";
            // 
            // LblTytulArtykulu
            // 
            this.LblTytulArtykulu.AutoSize = true;
            this.LblTytulArtykulu.Location = new System.Drawing.Point(231, 146);
            this.LblTytulArtykulu.Name = "LblTytulArtykulu";
            this.LblTytulArtykulu.Size = new System.Drawing.Size(74, 13);
            this.LblTytulArtykulu.TabIndex = 44;
            this.LblTytulArtykulu.Text = "Tytuł artykułu";
            // 
            // LblRokWydaniaCzasopisma
            // 
            this.LblRokWydaniaCzasopisma.AutoSize = true;
            this.LblRokWydaniaCzasopisma.Location = new System.Drawing.Point(231, 229);
            this.LblRokWydaniaCzasopisma.Name = "LblRokWydaniaCzasopisma";
            this.LblRokWydaniaCzasopisma.Size = new System.Drawing.Size(127, 13);
            this.LblRokWydaniaCzasopisma.TabIndex = 45;
            this.LblRokWydaniaCzasopisma.Text = "Rok wydania czasopisma";
            // 
            // TxtImieFilm
            // 
            this.TxtImieFilm.Location = new System.Drawing.Point(492, 81);
            this.TxtImieFilm.Name = "TxtImieFilm";
            this.TxtImieFilm.Size = new System.Drawing.Size(189, 20);
            this.TxtImieFilm.TabIndex = 46;
            // 
            // TxtNazwiskoFilm
            // 
            this.TxtNazwiskoFilm.Location = new System.Drawing.Point(492, 123);
            this.TxtNazwiskoFilm.Name = "TxtNazwiskoFilm";
            this.TxtNazwiskoFilm.Size = new System.Drawing.Size(189, 20);
            this.TxtNazwiskoFilm.TabIndex = 47;
            // 
            // TxtTytulFilm
            // 
            this.TxtTytulFilm.Location = new System.Drawing.Point(492, 160);
            this.TxtTytulFilm.Name = "TxtTytulFilm";
            this.TxtTytulFilm.Size = new System.Drawing.Size(189, 20);
            this.TxtTytulFilm.TabIndex = 48;
            // 
            // TxtMiejsceProdukcji
            // 
            this.TxtMiejsceProdukcji.Location = new System.Drawing.Point(492, 205);
            this.TxtMiejsceProdukcji.Name = "TxtMiejsceProdukcji";
            this.TxtMiejsceProdukcji.Size = new System.Drawing.Size(189, 20);
            this.TxtMiejsceProdukcji.TabIndex = 49;
            // 
            // LblInicjalFilm
            // 
            this.LblInicjalFilm.AutoSize = true;
            this.LblInicjalFilm.Location = new System.Drawing.Point(489, 65);
            this.LblInicjalFilm.Name = "LblInicjalFilm";
            this.LblInicjalFilm.Size = new System.Drawing.Size(71, 13);
            this.LblInicjalFilm.TabIndex = 50;
            this.LblInicjalFilm.Text = "Inicjał imienia";
            // 
            // LblNazwiskoFilm
            // 
            this.LblNazwiskoFilm.AutoSize = true;
            this.LblNazwiskoFilm.Location = new System.Drawing.Point(489, 107);
            this.LblNazwiskoFilm.Name = "LblNazwiskoFilm";
            this.LblNazwiskoFilm.Size = new System.Drawing.Size(53, 13);
            this.LblNazwiskoFilm.TabIndex = 51;
            this.LblNazwiskoFilm.Text = "Nazwisko";
            // 
            // LblTytulFilmu
            // 
            this.LblTytulFilmu.AutoSize = true;
            this.LblTytulFilmu.Location = new System.Drawing.Point(489, 146);
            this.LblTytulFilmu.Name = "LblTytulFilmu";
            this.LblTytulFilmu.Size = new System.Drawing.Size(56, 13);
            this.LblTytulFilmu.TabIndex = 52;
            this.LblTytulFilmu.Text = "Tytuł filmu";
            // 
            // FrmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(717, 449);
            this.Controls.Add(this.LblTytulFilmu);
            this.Controls.Add(this.LblNazwiskoFilm);
            this.Controls.Add(this.LblInicjalFilm);
            this.Controls.Add(this.TxtMiejsceProdukcji);
            this.Controls.Add(this.TxtTytulFilm);
            this.Controls.Add(this.TxtNazwiskoFilm);
            this.Controls.Add(this.TxtImieFilm);
            this.Controls.Add(this.LblRokWydaniaCzasopisma);
            this.Controls.Add(this.LblTytulArtykulu);
            this.Controls.Add(this.LblNazwiskoCzasopismo);
            this.Controls.Add(this.LblImieCzasopismo);
            this.Controls.Add(this.TxtRokWydaniaCzasopisma);
            this.Controls.Add(this.TxtTytulArtykulu);
            this.Controls.Add(this.TxtNazwiskoCz);
            this.Controls.Add(this.TxtImieCz);
            this.Controls.Add(this.TxtRokProdukcji);
            this.Controls.Add(this.BtnClean);
            this.Controls.Add(this.LblMiejsceProdukcji);
            this.Controls.Add(this.LblRokProdukcji);
            this.Controls.Add(this.LblNumerStronyCzasopisma);
            this.Controls.Add(this.TxtStronaCzasopisma);
            this.Controls.Add(this.LblTypNosnika);
            this.Controls.Add(this.RichPrzypis);
            this.Controls.Add(this.BtnTworzenie);
            this.Controls.Add(this.LblProducent);
            this.Controls.Add(this.LblNumerCzasopisma);
            this.Controls.Add(this.LblTytulCzasopisma);
            this.Controls.Add(this.LblNumerStronyKsiazki);
            this.Controls.Add(this.LblWydawnictwo);
            this.Controls.Add(this.LblRokWydania);
            this.Controls.Add(this.LblMiejsceWydania);
            this.Controls.Add(this.LblTytul);
            this.Controls.Add(this.LblNazwisko);
            this.Controls.Add(this.LblInicjal);
            this.Controls.Add(this.TxtNosnik);
            this.Controls.Add(this.TxtProd);
            this.Controls.Add(this.TxtNumerCzasopisma);
            this.Controls.Add(this.TxtTytulCzasopisma);
            this.Controls.Add(this.TxtStrona);
            this.Controls.Add(this.TxtWydawnictwo);
            this.Controls.Add(this.TxtRok);
            this.Controls.Add(this.TxtMiejsce);
            this.Controls.Add(this.TxtTytul);
            this.Controls.Add(this.TxtNazwisko);
            this.Controls.Add(this.TxtImie);
            this.Controls.Add(this.CmbWybór);
            this.Name = "FrmMenu";
            this.Text = "Bibliogram";
            this.Load += new System.EventHandler(this.FrmMenu_Load);
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.ComboBox CmbWybór;
        private System.Windows.Forms.TextBox TxtImie;
        private System.Windows.Forms.TextBox TxtNazwisko;
        private System.Windows.Forms.TextBox TxtTytul;
        private System.Windows.Forms.TextBox TxtMiejsce;
        private System.Windows.Forms.TextBox TxtRok;
        private System.Windows.Forms.TextBox TxtWydawnictwo;
        private System.Windows.Forms.TextBox TxtStrona;
        private System.Windows.Forms.TextBox TxtTytulCzasopisma;
        private System.Windows.Forms.TextBox TxtNumerCzasopisma;
        private System.Windows.Forms.TextBox TxtProd;
        private System.Windows.Forms.TextBox TxtNosnik;
        private System.Windows.Forms.Label LblInicjal;
        private System.Windows.Forms.Label LblNazwisko;
        private System.Windows.Forms.Label LblTytul;
        private System.Windows.Forms.Label LblMiejsceWydania;
        private System.Windows.Forms.Label LblRokWydania;
        private System.Windows.Forms.Label LblWydawnictwo;
        private System.Windows.Forms.Label LblNumerStronyKsiazki;
        private System.Windows.Forms.Label LblTytulCzasopisma;
        private System.Windows.Forms.Label LblNumerCzasopisma;
        private System.Windows.Forms.Label LblProducent;
        private System.Windows.Forms.Button BtnTworzenie;
        private System.Windows.Forms.RichTextBox RichPrzypis;
        private System.Windows.Forms.Label LblTypNosnika;
        private System.Windows.Forms.TextBox TxtStronaCzasopisma;
        private System.Windows.Forms.Label LblNumerStronyCzasopisma;
        private System.Windows.Forms.Label LblRokProdukcji;
        private System.Windows.Forms.Label LblMiejsceProdukcji;
        private System.Windows.Forms.Button BtnClean;
        private System.Windows.Forms.TextBox TxtRokProdukcji;
        private System.Windows.Forms.TextBox TxtImieCz;
        private System.Windows.Forms.TextBox TxtNazwiskoCz;
        private System.Windows.Forms.TextBox TxtTytulArtykulu;
        private System.Windows.Forms.TextBox TxtRokWydaniaCzasopisma;
        private System.Windows.Forms.Label LblImieCzasopismo;
        private System.Windows.Forms.Label LblNazwiskoCzasopismo;
        private System.Windows.Forms.Label LblTytulArtykulu;
        private System.Windows.Forms.Label LblRokWydaniaCzasopisma;
        private System.Windows.Forms.TextBox TxtImieFilm;
        private System.Windows.Forms.TextBox TxtNazwiskoFilm;
        private System.Windows.Forms.TextBox TxtTytulFilm;
        private System.Windows.Forms.TextBox TxtMiejsceProdukcji;
        private System.Windows.Forms.Label LblInicjalFilm;
        private System.Windows.Forms.Label LblNazwiskoFilm;
        private System.Windows.Forms.Label LblTytulFilmu;
    }
}

