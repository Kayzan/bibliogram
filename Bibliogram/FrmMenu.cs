﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bibliogram
{
    public partial class FrmMenu : Form
    {
        public FrmMenu()
        {
            InitializeComponent();
        }

        private void UstawTekstKsiazki(string tekst1a, string tekst2a, string tekst3a)
        {
            RichPrzypis.Clear(); // Czyscisz zawartosc

            RichPrzypis.SelectionFont = new Font("Times New Roman", 10, FontStyle.Regular);
            RichPrzypis.AppendText(tekst1a); // + zmienna1 
            
            RichPrzypis.SelectionFont = new Font("Times New Roman", 10, FontStyle.Italic);
            RichPrzypis.AppendText(tekst2a); // + zmienna2 + zmienna3

            RichPrzypis.SelectionFont = new Font("Times New Roman", 10, FontStyle.Regular);
            RichPrzypis.AppendText(tekst3a); // + zmienna1 

        }
        private void UstawTekstCzasopisma(string tekst1, string tekst2, string tekst3)
        {
            RichPrzypis.Clear(); // Czyscisz zawartosc
            RichPrzypis.SelectionFont = new Font("Times New Roman", 10, FontStyle.Regular);
            
            RichPrzypis.AppendText(tekst1); // + zmienna1 

            RichPrzypis.SelectionFont = new Font("Times New Roman", 10, FontStyle.Italic);
            RichPrzypis.AppendText(tekst2); // + zmienna2 + zmienna3

            RichPrzypis.SelectionFont = new Font(RichPrzypis.Font, FontStyle.Regular);
            RichPrzypis.AppendText(tekst3); // + zmienna1 

            TxtProd.Visible = false;

        }
        private void UstawTekstFilmu(string tekst1b, string tekst2b)
        {
            RichPrzypis.Clear(); // Czyscisz zawartosc

            RichPrzypis.SelectionFont = new Font("Times New Roman", 10, FontStyle.Italic);
            RichPrzypis.AppendText(tekst1b); // + zmienna1 

            RichPrzypis.SelectionFont = new Font("Times New Roman", 10, FontStyle.Regular);
            RichPrzypis.AppendText(tekst2b); // + zmienna2 + zmienna3

        }
        
        private void BtnTworzenie_Click(object sender, EventArgs e)
        {
            /*string imie = TxtImie.Text;
            string nazwisko = TxtNazwisko.Text;
            string tytul = TxtTytul.Text;
            string miejsce = TxtMiejsce.Text;
            //int rok = int.Parse(TxtRok.Text);
            string wydawnictwo = TxtWydawnictwo.Text;
            //int strona = int.Parse(TxtStrona.Text);
            string tytulczasopisma = TxtTytulCzasopisma.Text;
            //int numerczasopisma = int.Parse(TxtNumerCzasopisma.Text);
            string wynik = "";
            string producent = TxtProd.Text;
            string nosnik = TxtNosnik.Text;
            */
            string imieksiazka = TxtImie.Text;
            string nazwiskoksiazka = TxtNazwisko.Text;
            string tytulksiazka = TxtTytul.Text;
            string miejsceksiazka = TxtMiejsce.Text;
            string wydawnictwoksiazka = TxtWydawnictwo.Text;

            string imieczasopismo = TxtImieCz.Text;
            string nazwiskoczasopismo = TxtNazwiskoCz.Text;
            string tytulartykulu = TxtTytulArtykulu.Text;
            string nazwaczasopisma = TxtTytulCzasopisma.Text;

            string imiefilm = TxtImieFilm.Text;
            string nazwiskofilm = TxtNazwiskoFilm.Text;
            string tytulfilmu = TxtTytulFilm.Text;
            string miejsceprodukcji = TxtMiejsceProdukcji.Text;
            string producent = TxtProd.Text;
            string nosnik = TxtNosnik.Text;

            Wybor przypis = new Wybor();

            switch (CmbWybór.Text)
            {
                case "Książka":

                    var stronaksiazki =TxtStrona.Text;
                    int rokksiazka = int.Parse(TxtRok.Text);

                    // wynik = przypis.Książka(imie + ", ", nazwisko + ", ", tytul+ ", ", miejsce + " ", rok.ToString() + ", " +" wyd. ", wydawnictwo + ", " + "s.", strona.ToString() + ".");
                    UstawTekstKsiazki(imieksiazka + ". " + nazwiskoksiazka + ", ", tytulksiazka + ", ", miejsceksiazka + " " + rokksiazka.ToString() + ", " + " wyd. " + wydawnictwoksiazka + ", " + "s." + stronaksiazki.ToString() + ".");
                    break;

                case "Czasopismo":

                    var stronaczasopisma = TxtStronaCzasopisma.Text;
                    int numerczasopisma = int.Parse(TxtNumerCzasopisma.Text);
                    int RokCzasopismo = int.Parse(TxtRokWydaniaCzasopisma.Text);

                    UstawTekstCzasopisma(imieczasopismo + ". " + nazwiskoczasopismo + ", ", tytulartykulu + ", " , nazwaczasopisma + ", " + RokCzasopismo.ToString() + ", " + "nr " + numerczasopisma.ToString() + ", " + "s." + stronaczasopisma.ToString() + ".");
                    break;

                case "Film":
                    int RokProdukcji = int.Parse(TxtRokProdukcji.Text);

                    UstawTekstFilmu(tytulfilmu, ", " + "[film]" + ", " + imiefilm + "." + nazwiskofilm + ", " + miejsceprodukcji + ": " + producent + ", " + RokProdukcji.ToString() + ", " + nosnik);
                        break;
            }

        }

        private void RichPrzypis_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void CmbWybór_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (CmbWybór.Text == "")
            {
                TxtProd.Visible = false;
                TxtNosnik.Visible = false;
                TxtImie.Visible = false;
                TxtNazwisko.Visible = false;
                TxtTytul.Visible = false;
                TxtNumerCzasopisma.Visible = false;
                TxtStronaCzasopisma.Visible = false;
                TxtTytulCzasopisma.Visible = false;
                LblNumerCzasopisma.Visible = false;
                LblNumerStronyCzasopisma.Visible = false;
                LblProducent.Visible = false;
                LblTypNosnika.Visible = false;
                LblTytulCzasopisma.Visible = false;
                
            }
            else if (CmbWybór.Text == "Książka")
            {
                TxtImie.Visible = true;
                TxtNazwisko.Visible = true;
                TxtTytul.Visible = true;
                TxtMiejsce.Visible = true;
                TxtRok.Visible = true;
                TxtWydawnictwo.Visible = true;
                TxtStrona.Visible = true;
                LblInicjal.Visible = true;
                LblNazwisko.Visible = true;
                LblTytul.Visible = true;
                LblMiejsceWydania.Visible = true;
                LblRokWydania.Visible = true;
                LblWydawnictwo.Visible = true;
                LblNumerStronyKsiazki.Visible = true;

                TxtImieCz.Visible = false;
                TxtNazwiskoCz.Visible = false;
                TxtTytulArtykulu.Visible = false;
                TxtTytulCzasopisma.Visible = false;
                TxtRokWydaniaCzasopisma.Visible = false;
                TxtNumerCzasopisma.Visible = false;
                TxtStronaCzasopisma.Visible = false;
                LblImieCzasopismo.Visible = false;
                LblNazwiskoCzasopismo.Visible = false;
                LblTytulArtykulu.Visible = false;
                LblTytulCzasopisma.Visible = false;
                LblRokWydaniaCzasopisma.Visible = false;
                LblNumerCzasopisma.Visible = false;
                LblNumerStronyCzasopisma.Visible = false;

                TxtImieFilm.Visible = false;
                TxtNazwiskoFilm.Visible = false;
                TxtTytulFilm.Visible = false;
                TxtMiejsceProdukcji.Visible = false;
                TxtProd.Visible = false;
                TxtRokProdukcji.Visible = false;
                TxtNosnik.Visible = false;
                LblInicjalFilm.Visible = false;
                LblNazwiskoFilm.Visible = false;
                LblTytulFilmu.Visible = false;
                LblMiejsceProdukcji.Visible = false;
                LblProducent.Visible = false;
                LblRokProdukcji.Visible = false;
                LblTypNosnika.Visible = false;

            }
            else if (CmbWybór.Text == "Czasopismo")
            {
                TxtImie.Visible = false;
                TxtNazwisko.Visible = false;
                TxtTytul.Visible = false;
                TxtMiejsce.Visible = false;
                TxtRok.Visible = false;
                TxtWydawnictwo.Visible = false;
                TxtStrona.Visible = false;
                LblInicjal.Visible = false;
                LblNazwisko.Visible = false;
                LblTytul.Visible = false;
                LblMiejsceWydania.Visible = false;
                LblRokWydania.Visible = false;
                LblWydawnictwo.Visible = false;
                LblNumerStronyKsiazki.Visible = false;

                TxtImieCz.Visible = true;
                TxtNazwiskoCz.Visible = true;
                TxtTytulArtykulu.Visible = true;
                TxtTytulCzasopisma.Visible = true;
                TxtRokWydaniaCzasopisma.Visible = true;
                TxtNumerCzasopisma.Visible = true;
                TxtStronaCzasopisma.Visible = true;
                LblImieCzasopismo.Visible = true;
                LblNazwiskoCzasopismo.Visible = true;
                LblTytulArtykulu.Visible = true;
                LblTytulCzasopisma.Visible = true;
                LblRokWydaniaCzasopisma.Visible = true;
                LblNumerCzasopisma.Visible = true;
                LblNumerStronyCzasopisma.Visible = true;

                TxtImieFilm.Visible = false;
                TxtNazwiskoFilm.Visible = false;
                TxtTytulFilm.Visible = false;
                TxtMiejsceProdukcji.Visible = false;
                TxtProd.Visible = false;
                TxtRokProdukcji.Visible = false;
                TxtNosnik.Visible = false;
                LblInicjalFilm.Visible = false;
                LblNazwiskoFilm.Visible = false;
                LblTytulFilmu.Visible = false;
                LblMiejsceProdukcji.Visible = false;
                LblProducent.Visible = false;
                LblRokProdukcji.Visible = false;
                LblTypNosnika.Visible = false;
            }
            else if (CmbWybór.Text == "Film")
            {
                TxtImie.Visible = false;
                TxtNazwisko.Visible = false;
                TxtTytul.Visible = false;
                TxtMiejsce.Visible = false;
                TxtRok.Visible = false;
                TxtWydawnictwo.Visible = false;
                TxtStrona.Visible = false;
                LblInicjal.Visible = false;
                LblNazwisko.Visible = false;
                LblTytul.Visible = false;
                LblMiejsceWydania.Visible = false;
                LblRokWydania.Visible = false;
                LblWydawnictwo.Visible = false;
                LblNumerStronyKsiazki.Visible = false;

                TxtImieCz.Visible = false;
                TxtNazwiskoCz.Visible = false;
                TxtTytulArtykulu.Visible = false;
                TxtTytulCzasopisma.Visible = false;
                TxtRokWydaniaCzasopisma.Visible = false;
                TxtNumerCzasopisma.Visible = false;
                TxtStronaCzasopisma.Visible = false;
                LblImieCzasopismo.Visible = false;
                LblNazwiskoCzasopismo.Visible = false;
                LblTytulArtykulu.Visible = false;
                LblTytulCzasopisma.Visible = false;
                LblRokWydaniaCzasopisma.Visible = false;
                LblNumerCzasopisma.Visible = false;
                LblNumerStronyCzasopisma.Visible = false;

                TxtImieFilm.Visible = true;
                TxtNazwiskoFilm.Visible = true;
                TxtTytulFilm.Visible = true;
                TxtMiejsceProdukcji.Visible = true;
                TxtProd.Visible = true;
                TxtRokProdukcji.Visible = true;
                TxtNosnik.Visible = true;
                LblInicjalFilm.Visible = true;
                LblNazwiskoFilm.Visible = true;
                LblTytulFilmu.Visible = true;
                LblMiejsceProdukcji.Visible = true;
                LblProducent.Visible = true;
                LblRokProdukcji.Visible = true;
                LblTypNosnika.Visible = true;
            }
        }

        private void BtnClean_Click(object sender, EventArgs e)
        {
            
            TxtImie.Text = "";
            TxtNazwisko.Text = "";
            TxtTytul.Text = "";
            TxtMiejsce.Text = "";
            TxtWydawnictwo.Text = "";
            
            TxtImieCz.Text = "";
            TxtNazwiskoCz.Text = "";
            TxtTytulArtykulu.Text = "";
            TxtTytulCzasopisma.Text = "";

            TxtImieFilm.Text = "";
            TxtNazwiskoFilm.Text = "";
            TxtTytulFilm.Text = "";
            TxtMiejsceProdukcji.Text = "";
            TxtProd.Text = "";
            TxtNosnik.Text = "";

            TxtStrona.Text = "";
            TxtRok.Text = "";
            TxtStronaCzasopisma.Text = "";
            TxtRokProdukcji.Text = "";
            TxtRokWydaniaCzasopisma.Text = "";
            TxtNumerCzasopisma.Text = "";

        }

        private void FrmMenu_Load(object sender, EventArgs e)
        {

        }
    }
}
