﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bibliogram
{
    class Wybor
    {
        public string Książka(string imie, string nazwisko, string tytul, string miejsce, string rok, string wydawnictwo, string stronaksiazki)
        {
            return imie + nazwisko + tytul + miejsce + rok + wydawnictwo + stronaksiazki;
        }
        public string Czasopismo(string imie, string nazwisko, string tytul, string tytulczasopisma, string rok, string numerczasopisma, string stronaczasopisma)
        {
            return imie + nazwisko + tytul + tytulczasopisma + rok + numerczasopisma + stronaczasopisma;
        }
        public string Film(string tytul, string imie, string nazwisko, string miejsce, string producent, string rok, string nosnik)
        {
            return tytul + imie + nazwisko + miejsce + producent + rok + nosnik;
        }       
    }
}
